API Documentation
=================

.. module:: sairen

.. autoclass:: MarketEnv
   :members:
   :show-inheritance:
   :undoc-members:

.. We can't use automodule because we don't want :members: for namedtuples, since the auto docs are pretty useless.

.. autoclass:: Bar


